package com.example.saianushapenujuri.recyclerview_example;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DialogActivity.DialogListener {

    private RecyclerView mrecyclerView;

    private ArrayList<ListItem> mlist = new ArrayList<>();
    private DbHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mrecyclerView = findViewById(R.id.recyclerview);
        Button btn = findViewById(R.id.btnAdd);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogActivity dialog = new DialogActivity();
                dialog.show(getSupportFragmentManager(), "example dialog");
            }
        });

        MyAdapterDemo adapter = new MyAdapterDemo(mlist, this);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mrecyclerView.setAdapter(adapter);

    }

    @Override
    public ArrayList<ListItem> applyTexts(String name, String number, String email) {
        ListItem listItem = new ListItem();
        listItem.setName(name);
        listItem.setNumber(number);
        listItem.setEmail(email);
        mlist.add(listItem);
        return mlist;
    }


    private void display() {

        SQLiteDatabase db = helper.getReadableDatabase();
        String table = "stu_table";
        String[] columns = null;
        String selection = null;
        String[] selectionArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
        /*StringBuffer buffer = new StringBuffer();
        while (cursor.moveToNext()) {
            buffer.append("Name :" + cursor.getString(0) + "\n");
            buffer.append("Age :" + cursor.getString(1) + "\n");
        }
        ArrayList<String> list = new ArrayList<String>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            list.add(cursor.getString(cursor.getColumnIndex("name"))); //add the item
            cursor.moveToNext();
        }*/

        db.close();
    }
}
