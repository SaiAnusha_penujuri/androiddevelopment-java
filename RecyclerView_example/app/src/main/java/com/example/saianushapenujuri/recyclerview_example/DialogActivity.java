package com.example.saianushapenujuri.recyclerview_example;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class DialogActivity extends AppCompatDialogFragment {
    private EditText mNameEdit;
    private EditText mNumberEdit;
    private EditText mEmailEdit;
    private DbHelper helper;
    String username;
    String number;
    String email;
    private DialogListener dialogListener;
    private ArrayList<ListItem> mlist = new ArrayList<>();


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        helper = new DbHelper(getContext(), "my db", null, 1);
        final MyAdapterDemo adapter = new MyAdapterDemo(mlist, getContext());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);
        builder.setView(view).setTitle("Details").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delete(number);
                adapter.updateList(mlist);


            }
        })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        username = mNameEdit.getText().toString();
                        number = mNumberEdit.getText().toString();
                        email = mEmailEdit.getText().toString();
                        mlist=dialogListener.applyTexts(username, number, email);
                        insert(username, number,email);
                        mNumberEdit.setText("");
                        mNameEdit.setText("");
                        mEmailEdit.setText("");

                    }
                });
        mNameEdit = view.findViewById(R.id.edit_name);
        mNumberEdit = view.findViewById(R.id.edit_num);
        mEmailEdit = view.findViewById(R.id.edit_email);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            dialogListener = (DialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement");
        }
    }

    public interface DialogListener {
        ArrayList<ListItem> applyTexts(String name, String number, String email);
    }

    public void insert(String name, String number, String email) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("phonenumber", number);
        values.put("email", email);
        db.insert("Stu_Table", null, values);
        db.close();
    }

    private void delete(String number) {
        SQLiteDatabase db = helper.getWritableDatabase();
        String table = "Stu_Table";
        String whereClause = "phonenumber=?";
        String[] whereArgs = {number};
        db.delete(table, whereClause, whereArgs);
        db.close();

    }

}
