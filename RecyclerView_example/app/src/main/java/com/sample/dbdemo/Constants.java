package com.sample.dbdemo;

public class Constants {
    //Database Name
    public static final String DB_NAME = "STUDENT_DATABASE";

    //Database Fields
    public static final String NAME = "NAME";
    public static final String PHONE = "PHONE";
    public static final String EMAIL = "EMAIL";
}
