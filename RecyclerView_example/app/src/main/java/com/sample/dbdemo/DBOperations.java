package com.sample.dbdemo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DBOperations {

    private static final int NAME = 0;
    private static final int PHONE = 1;
    private static final int EMAIL = 2;

    public static void insert(DBHelper dbHelper, Student student) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.NAME, student.getName());
        values.put(Constants.PHONE, student.getPhoneNumber());
        values.put(Constants.EMAIL, student.getEmail());
        db.insert(Constants.DB_NAME, null, values);
        db.close();
    }

    public static ArrayList<Student> getData(DBHelper dbHelper) {
        ArrayList<Student> students = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(Constants.DB_NAME, null, null, null,
                null, null, null);
        while (cursor.moveToNext()) {
            Student student = new Student();
            student.setName(cursor.getString(NAME));
            student.setPhoneNumber(cursor.getLong(PHONE));
            student.setEmail(cursor.getString(EMAIL));
            students.add(student);
        }
        db.close();
        return students;
    }

    public static void deleteStudent(DBHelper dbHelper, String email) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(Constants.DB_NAME, Constants.EMAIL + "=?", new String[]{email});
        db.close();
    }

    public static void update(DBHelper dbHelper, Student student) {
        List<Student> students = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("UPDATE " + Constants.DB_NAME + " SET " + Constants.NAME + "=" + "'DEMO'" + " WHERE " + Constants.NAME + "=" + student.getName());
        db.close();
    }
}
