package com.sample.dbdemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.saianushapenujuri.recyclerview_example.DbHelper;
import com.example.saianushapenujuri.recyclerview_example.R;

import java.util.ArrayList;



public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    public ArrayList<Student> mStudents;
    private Context mContext;
    private ListItemClick mItemClick;
    private Button mDelete;



    interface ListItemClick {
        void itemClick(int position);
        void deleteItem(int position);
    }

    public MyAdapter(Context context, ListItemClick itemClick, ArrayList<Student> students) {
        this.mContext = context;
        this.mItemClick = itemClick;
        this.mStudents = students;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ViewHolder(view, mItemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, final int position) {
        final Student student = mStudents.get(position);

        viewHolder.name.setText(student.getName());
        viewHolder.number.setText("" + student.getPhoneNumber());
        viewHolder.email.setText(student.getEmail());
        viewHolder.mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClick.deleteItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStudents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name;
        public TextView number;
        public TextView email;
        public ListItemClick itemClick;
        public Button mDelete;

        public ViewHolder(@NonNull View itemView, final ListItemClick itemClick) {
            super(itemView);
            name = itemView.findViewById(R.id.text_name);
            number = itemView.findViewById(R.id.text_number);
            email = itemView.findViewById(R.id.text_email);

            this.itemClick = itemClick;
            itemView.setOnClickListener(this);
            mDelete=itemView.findViewById(R.id.btn_delete);
        }

        @Override
        public void onClick(View view) {
            itemClick.itemClick(getAdapterPosition());
        }
    }
}