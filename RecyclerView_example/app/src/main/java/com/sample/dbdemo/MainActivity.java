package com.sample.dbdemo;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.saianushapenujuri.recyclerview_example.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyAdapter.ListItemClick {
    private DBHelper mDBHelper;
    private ArrayList<Student> mStudents = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private Button mDelete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerview);
        mDBHelper = new DBHelper(this, "test", null, 1);

        Button buttonAdd = findViewById(R.id.btnAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        mStudents = DBOperations.getData(mDBHelper);

        mAdapter = new MyAdapter(MainActivity.this, MainActivity.this, mStudents);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }

    public void showDialog() {
        Button mButtonInsert;
        Button mButtonDelete;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_customdialog);

        final EditText mNameEdit = dialog.findViewById(R.id.edit_name);
        final EditText mNumberEdit = dialog.findViewById(R.id.edit_num);
        final EditText mEmailEdit = dialog.findViewById(R.id.edit_email);

        mButtonInsert = dialog.findViewById(R.id.btn_insert);
        mButtonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mUserName = mNameEdit.getText().toString();
                String mNumber = mNumberEdit.getText().toString();
                String mEmail = mEmailEdit.getText().toString();
                Student student = new Student();
                student.setName(mUserName);
                student.setPhoneNumber(Long.parseLong(mNumber));
                student.setEmail(mEmail);
                DBOperations.insert(mDBHelper, student);
                mStudents = DBOperations.getData(mDBHelper);
                mAdapter.mStudents = mStudents;
                mAdapter.notifyDataSetChanged();
                for (Student stu : mStudents) {
                    Log.e("data", "Name: " + stu.getName() + " " + stu.getEmail() + " " + stu.getPhoneNumber());
                }
                dialog.cancel();
            }
        });
        mButtonDelete = dialog.findViewById(R.id.btn_cancel_insert);
        mButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    @Override
    public void itemClick(final int position) {
        showDialogDelete(position);
    }

    public void deleteItem(final int position) {
        Button mDelete = findViewById(R.id.btn_delete);
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBOperations.deleteStudent(mDBHelper, mStudents.get(position).getEmail());
                mStudents = DBOperations.getData(mDBHelper);
                mAdapter.mStudents = mStudents;
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    public void showDialogDelete(final int position) {
        Button mButtonCancelD;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_dialogdelete);

        final TextView nameText = dialog.findViewById(R.id.text_name);
        final TextView numberText = dialog.findViewById(R.id.text_num);
        final TextView emailText = dialog.findViewById(R.id.text_email);
        String userName = mStudents.get(position).getName();
        Long number = mStudents.get(position).getPhoneNumber();
        final String email = mStudents.get(position).getEmail();
        nameText.setText("Name:" + userName);
        numberText.setText("Number:" + number);
        emailText.setText("Email:" + email);

        mButtonCancelD = dialog.findViewById(R.id.btn_cancel_delete);
        mButtonCancelD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
    }
}