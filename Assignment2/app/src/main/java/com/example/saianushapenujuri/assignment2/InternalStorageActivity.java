package com.example.saianushapenujuri.assignment2;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class InternalStorageActivity extends AppCompatActivity {
    private Button mSave, mLoad;
    private TextView mTextData;
    private EditText mEditData;

    private String mdata;
    private String mfile = "mydata.txt";
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internalstorage);

        mEditData = findViewById(R.id.edit_data);
        mTextData = findViewById(R.id.txt_data);
        mSave = findViewById(R.id.btn_save);
        mSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    file=getFilesDir();
                    mdata = mEditData.getText().toString();
                    FileOutputStream fOut = openFileOutput(mfile, Context.MODE_PRIVATE);
                    fOut.write(mdata.getBytes());
                    fOut.close();
                    Toast.makeText(getBaseContext(), "file saved"+file, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mLoad = findViewById(R.id.btn_load);
        mLoad.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    FileInputStream fin = openFileInput(mfile);
                    int c;
                    String temp = "";
                    while ((c = fin.read()) != -1) {
                        temp = temp + Character.toString((char) c);
                    }
                    mTextData.setText("file data:" + temp);
                    Toast.makeText(getBaseContext(), "file read", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                }
            }
        });
    }
}
