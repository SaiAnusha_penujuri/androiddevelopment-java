package com.example.saianushapenujuri.assignment2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button mSharedPreferences;
    private Button mInternalStorage;
    private Button mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSharedPreferences=findViewById(R.id.btn_SharedPreferences);
        mSharedPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, SharedPreferencesActivity.class);
                startActivity(intent);
            }
        });
        mInternalStorage=findViewById(R.id.btn_InternalStorage);
        mInternalStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, InternalStorageActivity.class);
                startActivity(intent);
            }
        });
        mRecyclerView=findViewById(R.id.btn_RecyclerView);
        mRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,CustomDialogActivity.class);
                startActivity(intent);

            }
        });
    }
}
