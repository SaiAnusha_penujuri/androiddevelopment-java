package com.example.saianushapenujuri.assignment2;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class CustomDialogActivity  extends AppCompatActivity implements DialogActivity.DialogListener {

    private RecyclerView mrecyclerView;

    private ArrayList<ListItem> mlist = new ArrayList<>();
    private DbHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customdialog);

        mrecyclerView = findViewById(R.id.recyclerview);
        Button btn = findViewById(R.id.btnAdd);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogActivity dialog = new DialogActivity();
                dialog.show(getSupportFragmentManager(), "example dialog");
            }
        });

        MyAdapter adapter = new MyAdapter(mlist, this);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mrecyclerView.setAdapter(adapter);

    }

    @Override
    public ArrayList<ListItem> applyTexts(String name, String number, String email) {
        ListItem listItem = new ListItem();
        listItem.setName(name);
        listItem.setNumber(number);
        listItem.setEmail(email);
        mlist.add(listItem);
        return mlist;
    }
}

