package com.example.saianushapenujuri.notes_app;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import petrov.kristiyan.colorpicker.ColorPicker;

public class AddNotesActivity extends AppCompatActivity {
    private EditText mTitleEdit;
    private EditText mNotesEdit;
    private DBHelper mDBHelper;
    private String titleStr;
    private String notesStr;
    private String currentDateTimeString;
    private RelativeLayout mRelativeLayout;
    private String colorPicked = "#ffffff";
    private ArrayList<String> colors = new ArrayList<>();
    private FloatingActionButton mFABSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);

        colors.add("#bc8f8f");
        colors.add("#63cbd3");
        colors.add("#2f76eb");
        colors.add("#3ce0fb");
        colors.add("#daa520");
        colors.add("#e0ff5a");
        colors.add("#a79de7");
        colors.add("#FFC0CB");
        colors.add("#ad805a");
        colors.add("#7cfc00");
        colors.add("#FF7F50");
        colors.add("#FF8C00");

        mDBHelper = new DBHelper(this, "test", null, 1);


        mTitleEdit = findViewById(R.id.edit_title);
        mNotesEdit = findViewById(R.id.edit_notes);
        mRelativeLayout = findViewById(R.id.relativeLayout);

        mFABSave = findViewById(R.id.imgbtn_save);
        mFABSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                titleStr = mTitleEdit.getText().toString();
                notesStr = mNotesEdit.getText().toString();

                Notes notes = new Notes();
                notes.setTitle(titleStr);
                notes.setNotes(notesStr);
                notes.setDate(currentDateTimeString);
                notes.setColor(colorPicked);

                String[] columns = {Constants.TITLE};
                String selection = Constants.TITLE + " =?";
                String[] selectionArgs = {notes.getTitle()};
                String limit = "1";
                SQLiteDatabase db = mDBHelper.getWritableDatabase();

                Cursor cursor = db.query(Constants.DB_NAME, columns, selection, selectionArgs, null, null, null, limit);
                if ((cursor.getCount() > 0)) {
                    Log.e("insert", "already");
                    Toast.makeText(getApplicationContext(), "Title name already exists", Toast.LENGTH_LONG).show();
                    cursor.close();
                } else if (!(titleStr.equals("") && notesStr.equals(""))) {
                    DBOperations.insert(mDBHelper, notes);
                    finish();

                } else {
                    Toast.makeText(getApplicationContext(), "Enter the notes", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void openColorPicker() {
        final ColorPicker colorPicker = new ColorPicker(this);
        colorPicker.setColors(colors)
                .setColumns(4)
                .setRoundColorButton(true)
                .setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                    @Override
                    public void onChooseColor(int position, int color) {

                        mRelativeLayout.setBackgroundColor(color);
                        colorPicked = colors.get(position);
                        Log.e("test", "color: " + colorPicked + ":" + position);

                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_color, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_color:
                openColorPicker();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}