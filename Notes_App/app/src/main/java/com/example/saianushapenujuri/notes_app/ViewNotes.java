package com.example.saianushapenujuri.notes_app;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ViewNotes extends AppCompatActivity {
    private ArrayList<Notes> mNotes = new ArrayList<>();
    private DBHelper mDBHelper;
    private int mPosition;
    private String mTitleStr;
    private String mNotesStr;
    private RelativeLayout mRelativeLayout;
    private Notes note=new Notes();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes_view);
        mDBHelper = new DBHelper(this, "test", null, 1);
        mPosition = getIntent().getExtras().getInt("POSITIONINT");

        TextView mTitleText = findViewById(R.id.text_title);
        TextView mNotesText = findViewById(R.id.text_notes);
        mRelativeLayout=findViewById(R.id.relativeLayout);
        mNotes = DBOperations.getData(mDBHelper);
        mTitleStr = mNotes.get(mPosition).getTitle();
        mNotesStr = mNotes.get(mPosition).getNotes();
        mTitleText.setText(mTitleStr);
        mNotesText.setText(mNotesStr);
        mRelativeLayout.setBackgroundColor(Color.parseColor(mNotes.get(mPosition).getColor()));

        FloatingActionButton mEdit = findViewById(R.id.btn_Edit);
        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewNotes.this, UpdateActivity.class);
                intent.putExtra("TITLE", mTitleStr);
                intent.putExtra("POSITION", mPosition);
                intent.putExtra("NOTES", mNotesStr);
                startActivity(intent);
                finish();
            }
        });

    }

    public void deleteNotes() {
        note=DBOperations.deleteList(mDBHelper,mPosition);
        DBOperations.deleteStudent(mDBHelper, mNotes.get(mPosition).getId(),mPosition);

        final Snackbar snackbar = Snackbar
                .make(mRelativeLayout, "Item is deleted", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBOperations.undo(mDBHelper,note);
            }
        });
        snackbar.show();
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                showAlertDialog();
                return true;
            case R.id.action_share:
                shareData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void shareData() {
        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareBody=mNotesStr;
        String shareSub=mTitleStr;
        intent.putExtra(Intent.EXTRA_TEXT,shareSub+" "+shareBody);
        startActivity(Intent.createChooser(intent,"share using"));
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Are you sure want to delete?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteNotes();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

