package com.example.saianushapenujuri.notes_app;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    public ArrayList<Notes> mNotes;
    private ListItemClick mItemClick;

    interface ListItemClick {
        void itemClick(int position);
        void deleteItem(int position);
    }

    MyAdapter(Context context, ListItemClick itemClick, ArrayList<Notes> notes) {
        Context mContext = context;
        this.mItemClick = itemClick;
        this.mNotes = notes;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notes_item, viewGroup, false);
        return new ViewHolder(view, mItemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, final int position) {
        final Notes note = mNotes.get(position);

        viewHolder.titleText.setText(note.getTitle());
        viewHolder.notesText.setText(note.getNotes());
        viewHolder.dateText.setText(note.getDate());
        viewHolder.cardItem.setBackgroundColor(Color.parseColor(note.getColor()));
        Log.e("test", "color: " + note.getColor());

        viewHolder.deleteImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClick.deleteItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titleText;
        public TextView notesText;
        public TextView dateText;

        public ListItemClick itemClick;
        public ImageView deleteImgBtn;
        public CardView cardItem;


        public ViewHolder(@NonNull View itemView, final ListItemClick itemClick) {
            super(itemView);
            titleText = itemView.findViewById(R.id.text_title);
            notesText = itemView.findViewById(R.id.text_notes);
            dateText = itemView.findViewById(R.id.text_date);
            cardItem = itemView.findViewById(R.id.item_card);

            deleteImgBtn = itemView.findViewById(R.id.imgbtn_delete);
            this.itemClick = itemClick;
            itemView.setOnClickListener(this);

        }

        public void onClick(View view) {
            itemClick.itemClick(getAdapterPosition());
        }

    }
}
