package com.example.saianushapenujuri.notes_app;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyAdapter.ListItemClick {
    private ArrayList<Notes> mNotesList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private DBHelper mDBHelper;
    private int id;
    private TextView mTextMessage;
    private RelativeLayout mRelativeLayout;
    private Notes note = new Notes();
    private FloatingActionButton mAddFloatingAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.notes_RecyclerView);
        mDBHelper = new DBHelper(this, "test", null, 1);
        mTextMessage = findViewById(R.id.text_message);
        mRelativeLayout = findViewById(R.id.relativeLayout);


        mAddFloatingAction = findViewById(R.id.AddFloatingAction);
        mAddFloatingAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextActivity();

            }
        });
        mNotesList = DBOperations.getData(mDBHelper);
        mAdapter = new MyAdapter(MainActivity.this, MainActivity.this, mNotesList);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
        updateMessage();
    }

    @Override
    public void itemClick(final int position) {
        showNotes(position);
    }

    public void deleteItem(final int position) {
        id = mNotesList.get(position).getId();
        mNotesList = DBOperations.getData(mDBHelper);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure want to delete?");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteNotes(position);
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert1 = builder.create();
        alert1.show();
    }


    public void deleteNotes(int position) {
        note = DBOperations.deleteList(mDBHelper, position);
        DBOperations.deleteStudent(mDBHelper, id, position);

        final Snackbar snackbar = Snackbar
                .make(mRelativeLayout, "Item is deleted", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBOperations.undo(mDBHelper, note);
                refreshList();
            }
        });
        snackbar.show();

        mNotesList = DBOperations.getData(mDBHelper);
        refreshList();
        updateMessage();
    }


    public void nextActivity() {
        Intent intent = new Intent(MainActivity.this, AddNotesActivity.class);
        startActivity(intent);
    }

    public void showNotes(int position) {
        Intent intent = new Intent(MainActivity.this, ViewNotes.class);
        intent.putExtra("POSITIONINT", position);
        startActivity(intent);
    }

    private void updateMessage() {
        mRecyclerView.setVisibility((mNotesList.isEmpty()) ? View.GONE : View.VISIBLE);
        mTextMessage.setVisibility((mNotesList.isEmpty()) ? View.VISIBLE : View.GONE);
    }

    private void refreshList() {
        if (mAdapter != null) {
            mNotesList = DBOperations.getData(mDBHelper);
            mAdapter.mNotes = mNotesList;
            mAdapter.notifyDataSetChanged();
        }
    }
}
