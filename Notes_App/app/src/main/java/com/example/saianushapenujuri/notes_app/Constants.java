package com.example.saianushapenujuri.notes_app;


public class Constants {
    //Database Name
    public static final String DB_NAME = "NOTES_DATABASE";

    //Database Fields
    public static final String ID="ID";
    public static final String TITLE = "TITLE";
    public static final String NOTES = "NOTES";
    public static final String DATE = "DATE";
    public static final String COLOR="COLOR";
}
