package com.example.saianushapenujuri.notes_app;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DBOperations {
    private static final int ID = 0;
    private static final int TITLE = 1;
    private static final int NOTES = 2;
    private static final int DATE = 3;
    private static final int COLOR = 4;

    public static void insert(DBHelper dbHelper, Notes notes) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.TITLE, notes.getTitle());
        values.put(Constants.NOTES, notes.getNotes());
        values.put(Constants.DATE, notes.getDate());
        values.put(Constants.COLOR, notes.getColor());
        db.insert(Constants.DB_NAME, null, values);

        db.close();
    }

    public static void undo(DBHelper dbHelper, Notes notes) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.ID, notes.getId());
        values.put(Constants.TITLE, notes.getTitle());
        values.put(Constants.NOTES, notes.getNotes());
        values.put(Constants.DATE, notes.getDate());
        values.put(Constants.COLOR, notes.getColor());
        db.insert(Constants.DB_NAME, null, values);

        db.close();
    }

    public static void updateData(DBHelper dbHelper, Notes notes, int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues row = new ContentValues();
        row.put(Constants.TITLE, notes.getTitle());
        row.put(Constants.NOTES, notes.getNotes());
        row.put(Constants.DATE, notes.getDate());
        row.put(Constants.COLOR, notes.getColor());
        db.update(Constants.DB_NAME, row, "id = ?", new String[]{String.valueOf(id)});
        db.close();
    }

    public static ArrayList<Notes> getData(DBHelper dbHelper) {
        ArrayList<Notes> notes = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(Constants.DB_NAME, null, null, null,
                null, null, null);
        while (cursor.moveToNext()) {
            Notes note = new Notes();
            note.setId(cursor.getInt(ID));
            note.setTitle(cursor.getString(TITLE));
            note.setNotes(cursor.getString(NOTES));
            note.setDate(cursor.getString(DATE));
            note.setColor(cursor.getString(COLOR));
            notes.add(note);
        }
        db.close();
        return notes;
    }

    public static Notes deleteList(DBHelper dbHelper, int position) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        ArrayList<Notes> mNotes = new ArrayList<>();
        mNotes = DBOperations.getData(dbHelper);
        Notes note1 = new Notes();
        note1.setId(mNotes.get(position).getId());
        note1.setTitle(mNotes.get(position).getTitle());
        note1.setNotes(mNotes.get(position).getNotes());
        note1.setColor(mNotes.get(position).getColor());
        note1.setDate(mNotes.get(position).getDate());
        db.close();
        return note1;
    }

    public static void deleteStudent(DBHelper dbHelper, int id, int position) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(Constants.DB_NAME, "id = ?", new String[]{String.valueOf(id)});
        db.close();
    }
}

