package com.example.saianushapenujuri.notes_app;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import petrov.kristiyan.colorpicker.ColorPicker;

public class UpdateActivity extends AppCompatActivity {
    private DBHelper mDBHelper;
    private EditText title_update_edit;
    private EditText notes_update_edit;
    private String strTitle;
    private String strNotes;
    private String updateTitle;
    private String updateNotes;
    private FloatingActionButton mFABSave;
    private int mPosition;
    private ArrayList<Notes> mNotes = new ArrayList<>();
    private String currentDateTimeString;
    private RelativeLayout mRelativeLayout;
    private String colorPicked;

    private ArrayList<String> colors = new ArrayList<>();
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);

        colors.add("#bc8f8f");
        colors.add("#63cbd3");
        colors.add("#2f76eb");
        colors.add("#3ce0fb");
        colors.add("#daa520");
        colors.add("#e0ff5a");
        colors.add("#a79de7");
        colors.add("#FFC0CB");
        colors.add("#ad805a");
        colors.add("#7cfc00");
        colors.add("#FF7F50");
        colors.add("#FF8C00");


        mDBHelper = new DBHelper(this, "test", null, 1);

        title_update_edit = findViewById(R.id.edit_title);
        notes_update_edit = findViewById(R.id.edit_notes);
        mRelativeLayout = findViewById(R.id.relativeLayout);


        strTitle = getIntent().getExtras().getString("TITLE");
        strNotes = getIntent().getExtras().getString("NOTES");
        mPosition = getIntent().getExtras().getInt("POSITION");
        mNotes = DBOperations.getData(mDBHelper);
        mRelativeLayout.setBackgroundColor(Color.parseColor(mNotes.get(mPosition).getColor()));
        colorPicked = mNotes.get(mPosition).getColor();


        title_update_edit.setText(strTitle);
        notes_update_edit.setText(strNotes);
        mFABSave = findViewById(R.id.imgbtn_save);
        mFABSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notes notes = new Notes();
                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

                updateTitle = title_update_edit.getText().toString();
                updateNotes = notes_update_edit.getText().toString();
                notes.setTitle(updateTitle);
                notes.setNotes(updateNotes);
                notes.setDate(currentDateTimeString);
                notes.setColor(colorPicked);

                mNotes = DBOperations.getData(mDBHelper);
                id = mNotes.get(mPosition).getId();
                DBOperations.updateData(mDBHelper, notes, id);
                finish();
            }
        });

    }

    public void openColorPicker() {
        final ColorPicker colorPicker = new ColorPicker(this);
        colorPicker.setColors(colors)
                .setColumns(4)
                .setRoundColorButton(true)
                .setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                    @Override
                    public void onChooseColor(int position, int color) {
                        mRelativeLayout.setBackgroundColor(color);
                        colorPicked = colors.get(position);
                        Log.e("test", "color: " + colorPicked + ":" + position);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_color, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_color:
                openColorPicker();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
