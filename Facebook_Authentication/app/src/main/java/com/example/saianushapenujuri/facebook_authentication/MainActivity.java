package com.example.saianushapenujuri.facebook_authentication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;


public class MainActivity extends AppCompatActivity {
    private TextView mEmailText;
    private CallbackManager callbackManager;
    private ImageView mProfileimgview;
    private ProgressDialog mDialog;
    private TextView mUserNameText;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callbackManager = CallbackManager.Factory.create();

        LoginButton mLoginButton = findViewById(R.id.btn_login);
        mEmailText = findViewById(R.id.text_email);
        mProfileimgview = findViewById(R.id.imgview_profile);
        mUserNameText = findViewById(R.id.text_username);

        mLoginButton.setReadPermissions(Arrays.asList("public_profile", "email"));


        mLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mDialog = new ProgressDialog(MainActivity.this);
                mDialog.setMessage("Retrieving data");
                mDialog.show();


                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        mDialog.dismiss();

                        Log.e("response", response.toString());
                        getData(object);

                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,id,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                mEmailText.setText("");
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken2) {
                if (accessToken == null) {
                    mEmailText.setText(AccessToken.getCurrentAccessToken().getUserId());

                } else if (accessToken2 == null) {
                    mUserNameText.setText("");
                    mEmailText.setText("");
                    mProfileimgview.setImageDrawable(null);

                }
            }
        };

    }

    private void getData(JSONObject object) {
        try {
            URL profile_picture = new URL("https://graph.facebook.com/" + object.getString("id") + "/picture?width=250&height=250");
            Picasso.with(this).load(profile_picture.toString()).into(mProfileimgview);
            String first_name = object.getString("first_name");
            String last_name = object.getString("last_name");
            mUserNameText.setText(first_name + " " + last_name);
            mEmailText.setText(object.getString("email"));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
