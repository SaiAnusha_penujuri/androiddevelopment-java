package com.example.saianushapenujuri.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

public class CreateProfileActivity extends AppCompatActivity implements
        AdapterView.OnItemSelectedListener {
    private Spinner mSkills;
    private EditText mEditName;
    private EditText mEditNumber;
    private EditText mEditEmail;
    private RadioButton mRadioButtonGender;
    private Button mSubmit;
    private RadioGroup mRadioGroupGender;
    private CheckBox mEnglish,mHindi;
    //private ToggleButton mToggle;
    private Switch mToggle;

    private String mStringGender;
    private String mStringName;
    private String mStringEmail;
    private String mStringNumber;
    public String mSkill;
    private String mStringLanguage;
    private String mStringHindi;
    private String mStringEnglish;
    private String mStringToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);

        mEditName = findViewById(R.id.edit_name);
        mEditEmail = findViewById(R.id.edit_email);
        mEditNumber = findViewById(R.id.edit_phone_number);
        mSubmit = findViewById(R.id.button_submit);
        mRadioGroupGender = findViewById(R.id.radiogroup_gender);
        mSkills = findViewById(R.id.skills);
        //mToggle = findViewById(R.id.togglebutton);
        mToggle = (Switch) findViewById(R.id.toggleswitch);

        mSkills.setOnItemSelectedListener(this);
        List<String> skillList = new ArrayList<String>();
        skillList.add("c");
        skillList.add("java");
        skillList.add("html");
        mEnglish=findViewById(R.id.english);
        mHindi=findViewById(R.id.hindi);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, skillList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSkills.setAdapter(dataAdapter);

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = mRadioGroupGender.getCheckedRadioButtonId();
                mRadioButtonGender = findViewById(selectedId);

                mStringName = mEditName.getText().toString();
                mStringNumber = mEditNumber.getText().toString();
                mStringEmail = mEditEmail.getText().toString();

                if(mEnglish.isChecked()){
                    mStringEnglish="English";
                }
                else{
                    mStringEnglish="";
                }
                if(mHindi.isChecked()){
                    mStringHindi="Hindi";
                }
                else{
                    mStringHindi="";
                }
                mStringLanguage=mStringEnglish+" "+mStringHindi;

                if (mToggle.isChecked())
                    mStringToggle = mToggle.getTextOn().toString();
                else
                    mStringToggle = mToggle.getTextOff().toString();


                if (isValidEmail(mStringEmail)
                        && !TextUtils.isEmpty(mStringName)
                        && isValidPhoneNumber(mStringNumber)
                        && isCheckBoxChecked(mStringGender)) {
                    Intent intent = new Intent(CreateProfileActivity.this, ProfileActivity.class);
                    intent.putExtra("NAME", mStringName);
                    intent.putExtra("EMAIL", mStringEmail);
                    intent.putExtra("NUMBER", mStringNumber);
                    intent.putExtra("GENDER", mRadioButtonGender.getText());
                    intent.putExtra("SKILL",mSkill);
                    intent.putExtra("LANGUAGE_KNOWN",mStringLanguage);
                    intent.putExtra("TOGGLE",mStringToggle);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateProfileActivity.this);
                    builder.setMessage("enter valid details")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        mSkill=item;
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public static boolean isValidEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return phoneNumber.length() == 10;
    }

    public static boolean isCheckBoxChecked(String mRadioGroupGender) {
        if (mRadioGroupGender == " ")
            return false;
        else
            return true;
    }
}
