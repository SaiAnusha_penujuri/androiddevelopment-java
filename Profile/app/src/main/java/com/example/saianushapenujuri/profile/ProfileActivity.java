package com.example.saianushapenujuri.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;


public class ProfileActivity extends AppCompatActivity {
    private TextView mTextName;
    private TextView mTextEmail;
    private TextView mTextNumber;
    private TextView mTextSkill;
    private TextView mTextToggle;
    private TextView mTextLanguage;
    private ImageView mGenderImage;

    private String mStringName;
    private String mStringLanguage;
    private String mStringEmail;
    private String mStringNumber;
    private String mStringGender;
    private String mStringToggle;
    private String mStringSKill;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mTextName = findViewById(R.id.text_name);
        mTextEmail = findViewById(R.id.text_email);
        mTextNumber = findViewById(R.id.text_number);
        mGenderImage = findViewById(R.id.gender_image);
        mTextSkill=findViewById(R.id.text_skill);
        mTextLanguage=findViewById(R.id.text_language);
        mTextToggle=findViewById(R.id.text_toggle);

        mStringName = getIntent().getExtras().getString("NAME");
        mStringEmail = getIntent().getExtras().getString("EMAIL");
        mStringNumber = getIntent().getExtras().getString("NUMBER");
        mStringGender = getIntent().getExtras().getString("GENDER");
        mStringToggle=getIntent().getExtras().getString("TOGGLE");
        mStringSKill=getIntent().getExtras().getString("SKILL");
        mStringLanguage=getIntent().getExtras().getString("LANGUAGE_KNOWN");


        if (mStringGender.equals("male")) {
            mGenderImage.setImageResource(R.drawable.male);
        } else {
            mGenderImage.setImageResource(R.drawable.female);
        }

        mTextName.setText(mStringName);
        mTextNumber.setText(mStringNumber);
        mTextEmail.setText(mStringEmail);
        mTextSkill.setText("Skills:"+mStringSKill);
        mTextLanguage.setText("Languages known:"+mStringLanguage);
        mTextToggle.setText("Indian:"+mStringToggle);

    }
}
