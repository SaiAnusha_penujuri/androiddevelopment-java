package com.example.saianushapenujuri.json_details;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {

    private Context mContext;
    private List<Details> mListDetails;

    DetailsAdapter(Context context, List<Details> list) {
        this.mContext = context;
        this.mListDetails = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.single_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Details details = mListDetails.get(position);

        holder.textName.setText(details.getName());
        holder.textPassword.setText(details.getPassword());
        holder.textContact.setText(details.getContact());
        holder.textCountry.setText(details.getCountry());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "you clicked " + details.getName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListDetails.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        private TextView textPassword;
        TextView textContact;
        TextView textCountry;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.text_name);
            textPassword = itemView.findViewById(R.id.text_password);
            textContact = itemView.findViewById(R.id.text_contact);
            textCountry = itemView.findViewById(R.id.text_country);
            linearLayout = itemView.findViewById(R.id.linearlayout);
        }
    }

}
