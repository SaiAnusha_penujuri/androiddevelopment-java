package com.example.saianushapenujuri.assignment;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button mImplicit;
    private Button mExplicit;
    private Button mShare;
    private Button mService;
    private Button mSqlite;
    private Button mBroadCastReceiver;
    private Button mContentProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImplicit = findViewById(R.id.btn_implicit);
        mImplicit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.gmail.com"));
                startActivity(intent);

            }
        });

        mExplicit = findViewById(R.id.btn_explicit);
        mExplicit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ExplicitActivity.class);
                startActivity(intent);

            }
        });

        mService = findViewById(R.id.btn_service);
        mService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ServiceActivity.class);
                startActivity(intent);
            }
        });

        mSqlite = findViewById(R.id.btn_sqlite);
        mSqlite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SqliteActivity.class);
                startActivity(intent);
            }
        });

        mShare = findViewById(R.id.btn_share);
        mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ShareActivity.class);
                startActivity(intent);
            }
        });

        mBroadCastReceiver = findViewById(R.id.btn_broadcastreceiver);
        mBroadCastReceiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BroadCastActivity.class);
                startActivity(intent);
            }
        });

        mContentProvider=findViewById(R.id.btn_contentprovider);
        mContentProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ContentProviderActivity.class);
                startActivity(intent);
            }
        });
    }
}
