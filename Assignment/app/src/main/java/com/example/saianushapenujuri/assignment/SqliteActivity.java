package com.example.saianushapenujuri.assignment;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SqliteActivity extends AppCompatActivity {

    private Button mInsert;
    private Button mUpdate;
    private Button mDisplay;
    private Button mDelete;
    private DbHelper helper;
    private TextView mInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);
        mInfo = findViewById(R.id.text_info);

        helper = new DbHelper(this, "mydb", null, 1);
        mInsert = findViewById(R.id.btn_insert);
        mInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insert();
            }
        });
        mDelete = findViewById(R.id.btn_delete);
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete();
            }
        });
        mDisplay = findViewById(R.id.btn_display);
        mDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display();
            }
        });
        mUpdate = findViewById(R.id.btn_update);
        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });
    }

    private void insert() {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", getName());
        values.put("age", getAge());
        db.insert("Stu_Table", null, values);
        Toast.makeText(getApplicationContext(), "data inserted", Toast.LENGTH_SHORT).show();
        db.close();
    }

    private void update() {
        SQLiteDatabase db = helper.getWritableDatabase();
        String table = "Stu_Table";
        ContentValues values = new ContentValues();
        values.put("name", getName());
        String whereClause = "age=?";
        String[] whereArgs = {String.valueOf(getAge())};
        db.update(table, values, whereClause, whereArgs);
        Toast.makeText(getApplicationContext(), "data updated", Toast.LENGTH_SHORT).show();
        db.close();

    }

    private void delete() {
        SQLiteDatabase db = helper.getWritableDatabase();
        String table = "Stu_Table";
        String whereClause = "age=?";
        String[] whereArgs = {String.valueOf(getAge())};
        db.delete(table, whereClause, whereArgs);
        Toast.makeText(getApplicationContext(), "data deleted", Toast.LENGTH_SHORT).show();
        db.close();

    }

    private void display() {
        mInfo.setText(" ");
        SQLiteDatabase db = helper.getReadableDatabase();
        String table = "stu_table";
        String[] columns = null;
        String selection = null;
        String[] selectionArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);

        StringBuffer buffer = new StringBuffer();
        while (cursor.moveToNext()) {
            buffer.append("Name :" + cursor.getString(0) + "\n");
            buffer.append("Age :" + cursor.getString(1) + "\n");
        }
        mInfo.setText(buffer.toString());
        db.close();
    }

    private String getName() {
        return ((EditText) findViewById(R.id.edit_name)).getText().toString();
    }

    private Integer getAge() {
        return Integer.parseInt(((EditText) findViewById(R.id.edit_age)).getText().toString());
    }

}
