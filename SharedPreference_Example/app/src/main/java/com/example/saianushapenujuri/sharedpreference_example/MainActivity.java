package com.example.saianushapenujuri.sharedpreference_example;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText mName;
    private EditText mPassword;
    private Button mSave;
    private Button mNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mName = findViewById(R.id.edit_name);
        mPassword = findViewById(R.id.edit_pwd);
        mSave = findViewById(R.id.btn_save);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("Mydata", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("name", mName.getText().toString());
                editor.putString("password", mPassword.getText().toString());
                editor.commit();
                Toast.makeText(MainActivity.this, "data saved", Toast.LENGTH_LONG).show();
            }
        });

        mNext = findViewById(R.id.btn_next);
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityNext.class);
                Toast.makeText(MainActivity.this, "next", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
    }
}
