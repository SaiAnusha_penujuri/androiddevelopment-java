package com.example.saianushapenujuri.fragment_name;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Fragment_Display extends Fragment {
    TextView mTextName;
    TextView mTextEmail;
    ImageView mImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_display, container, false);
        mTextName = view.findViewById(R.id.text_name);
        mTextEmail = view.findViewById(R.id.text_email);
        mImage = view.findViewById(R.id.image);
        return view;
    }

    public void updateEditText(int imageid, String username, String email) {
        mImage.setImageResource(imageid);
        mTextName.setText(username);
        mTextEmail.setText(email);
    }

}
