package com.example.saianushapenujuri.fragment_name;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements Fragment_RecyclerView.FragmentRecyclerViewListener {
    private Fragment_Display mFragmentdisplay;
    private Fragment_RecyclerView mFragmentRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentdisplay = new Fragment_Display();
        mFragmentRecyclerView = new Fragment_RecyclerView();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_first, mFragmentdisplay)
                .replace(R.id.container_second, mFragmentRecyclerView)
                .commit();
    }

    public void onInputFirstSent(int imageid, String username, String email) {
        mFragmentdisplay.updateEditText(imageid, username, email);
    }
}

