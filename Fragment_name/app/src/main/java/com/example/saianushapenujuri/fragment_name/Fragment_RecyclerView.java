package com.example.saianushapenujuri.fragment_name;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class Fragment_RecyclerView extends Fragment implements DetailsAdapter.ListItemClick {
    private RecyclerView mRecyclerView;
    private ArrayList<User> mlist = new ArrayList<>();
    private FragmentRecyclerViewListener listener;
    public String mUserName = "name";
    public String mEmail = "email";
    public int mImageId;

    public interface FragmentRecyclerViewListener {
        void onInputFirstSent(int imageId, String name, String email);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        mRecyclerView = view.findViewById(R.id.recyclerview);

        DetailsAdapter contactsAdapter = new DetailsAdapter(getContext(), Fragment_RecyclerView.this, mlist);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(contactsAdapter);
        usersList();
        return view;
    }

    private void usersList() {
        User user = new User(R.drawable.male, "user1", "user1@gmail.com");
        mlist.add(user);

        user = new User(R.drawable.female, "user2", "user2@gmail.com");
        mlist.add(user);

        user = new User(R.mipmap.ic_launcher, "user3", "user3@gmail.com");
        mlist.add(user);
    }

    public void itemClick(final int position) {
        Log.e("data", "item clicked");
        sendDetails(position);
    }

    public void sendDetails(int position) {
        mUserName = mlist.get(position).getUserName();
        mEmail = mlist.get(position).getEmailId();
        mImageId = mlist.get(position).getImageId();
        listener.onInputFirstSent(mImageId, mUserName, mEmail);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentRecyclerViewListener) {
            listener = (FragmentRecyclerViewListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentFirstListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
