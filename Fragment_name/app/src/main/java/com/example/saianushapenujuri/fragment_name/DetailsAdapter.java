package com.example.saianushapenujuri.fragment_name;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {
    ArrayList<User> users = new ArrayList<>();
    private Context mContext;
    private ListItemClick mItemClick;


    interface ListItemClick {
        void itemClick(int position);
    }

    public DetailsAdapter(Context context, ListItemClick itemClick, ArrayList<User> users) {
        this.mContext = context;
        this.mItemClick = itemClick;
        this.users = users;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ViewHolder(view, mItemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textUserName.setText(users.get(i).userName);
        viewHolder.textEmail.setText(users.get(i).emailId);
        viewHolder.image.setImageResource(users.get(i).getImageId());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textUserName;
        public TextView textEmail;
        public ListItemClick itemClick;
        public ImageView image;


        public ViewHolder(@NonNull View itemView, final ListItemClick itemClick) {
            super(itemView);
            textUserName = itemView.findViewById(R.id.text_name);
            textEmail = itemView.findViewById(R.id.text_email);
            image = itemView.findViewById(R.id.image_profile_pic);

            this.itemClick = itemClick;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClick.itemClick(getAdapterPosition());
                }
            });
        }

    }
}

