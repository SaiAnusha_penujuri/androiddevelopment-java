package com.example.saianushapenujuri.fragment_name;

import android.widget.ImageView;

public class User {
    String userName;
    String emailId;
    int imageView;

    public User(int imageView, String userName, String emailId){
        this.imageView=imageView;
        this.userName=userName;
        this.emailId=emailId;
    }
    public int getImageId(){return imageView;}
    public void setImageId(int imageId){
        this.imageView=imageId;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
