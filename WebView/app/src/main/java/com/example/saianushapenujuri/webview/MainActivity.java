package com.example.saianushapenujuri.webview;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private WebView mWebView;
    private Button mAgreeBtn;
    private TextView mDisagreeTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        Boolean agreement = prefs.getBoolean("PrefConst", false);

        if (agreement == true) {
            setContentView(R.layout.activity_second);
        } else {
            setContentView(R.layout.activity_main);

            mWebView = findViewById(R.id.webview);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);


            WebViewActivity webViewClient = new WebViewActivity(this);
            mWebView.setWebViewClient(webViewClient);
            mWebView.loadUrl("https://www.google.com");

            mDisagreeTxt = findViewById(R.id.txt_disagree);

            Spannable spannable = new SpannableString("Disagree");
            spannable.setSpan(new UnderlineSpan(), 0, 8, 0);
            mDisagreeTxt.setText(spannable);

            mDisagreeTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    prefs.edit().putBoolean("PrefConst", false).apply();
                    finish();
                }
            });

            mAgreeBtn = findViewById(R.id.btn_agree);
            mAgreeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    prefs.edit().putBoolean("PrefConst", true).apply();
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    finish();
                    startActivity(intent);
                }
            });

        }
    }
}
