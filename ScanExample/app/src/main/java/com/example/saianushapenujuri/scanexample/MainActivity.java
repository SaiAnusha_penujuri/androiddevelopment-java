package com.example.saianushapenujuri.scanexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hp.jetadvantage.link.api.JetAdvantageLink;
import com.hp.jetadvantage.link.api.Result;
import com.hp.jetadvantage.link.api.SsdkUnsupportedException;
import com.hp.jetadvantage.link.api.job.JobInfo;
import com.hp.jetadvantage.link.api.job.JobService;
import com.hp.jetadvantage.link.api.scanner.ScanAttributesCaps;
import com.hp.jetadvantage.link.api.scanner.ScannerService;

public final class MainActivity extends AppCompatActivity {

    private Button mScanButton;
    private String mJobId = null;
    private JobInfo jobInfo = null;
    private final Result result = new Result();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            JetAdvantageLink.getInstance().initialize(this);
        } catch (SsdkUnsupportedException e) {
            e.printStackTrace();
        }
        mScanButton = findViewById(R.id.btn_scan);
        mScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new ScanToDestinationTask(MainActivity.this).execute();

            }
        });
    }

    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("jobId", mJobId);
        if (mJobId != null) {
            jobInfo = JobService.getJobInfo(getApplicationContext(), mJobId, result);
        }
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mJobId = savedInstanceState.getString("jobId");
    }
}
