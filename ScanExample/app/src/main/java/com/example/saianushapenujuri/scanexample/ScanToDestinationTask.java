package com.example.saianushapenujuri.scanexample;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.Log;

import com.hp.jetadvantage.link.api.CapabilitiesExceededException;
import com.hp.jetadvantage.link.api.Result;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes;
import com.hp.jetadvantage.link.api.scanner.ScanAttributesCaps;
import com.hp.jetadvantage.link.api.scanner.ScanletAttributes;
import com.hp.jetadvantage.link.api.scanner.ScannerService;

import java.lang.ref.WeakReference;

public class ScanToDestinationTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "[ScanSample]" + ScanToDestinationTask.class.getSimpleName();


    private final SharedPreferences mPrefs;
    private Context mContext;
    private String mErrorMsg = null;
    private ListPreference pref;
    private SharedPreferences mPref;

    public ScanToDestinationTask(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        mContext = context;
    }

    @Override
    protected Void doInBackground(final Void... params) {
        Result result=new Result();
        ScanAttributesCaps mCapabilities = ScannerService.getCapabilities(mContext, result);
        save(mCapabilities);
        final ScanletAttributes taskAttribs = new ScanletAttributes.Builder()
                .setShowSettingsUi(false)
                .build();
        ScannerService.submit(mContext, save(mCapabilities), taskAttribs);
        return null;
    }

    private ScanAttributes save(ScanAttributesCaps capabilities) {
        try {
            return new ScanAttributes.MeBuilder()
                    .setColorMode(ScanAttributes.ColorMode.DEFAULT)
                    .setDuplex(ScanAttributes.Duplex.DEFAULT)
                    .setDocumentFormat(ScanAttributes.DocumentFormat.DEFAULT)
                    .setScanSize(ScanAttributes.ScanSize.DEFAULT)
                    .setResolution(ScanAttributes.Resolution.DEFAULT)
                    .setOrientation(ScanAttributes.Orientation.DEFAULT)
                    .setScanPreview(ScanAttributes.ScanPreview.DEFAULT)
                    .setFileName("test")
                    .build(capabilities);
        } catch (CapabilitiesExceededException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(final Void aVoid) {
        super.onPostExecute(aVoid);

        if (mErrorMsg != null) {
            Log.e("error",mErrorMsg);

        }
    }
}

