
package com.hp.jetadvantage.link.sample.scansample.fragments;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.EditText;

import com.hp.jetadvantage.link.api.helper.email.Email;
import com.hp.jetadvantage.link.api.scanner.FileOptionsAttributes;
import com.hp.jetadvantage.link.api.scanner.FileOptionsAttributesCaps;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.ColorMode;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.DocumentFormat;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.Duplex;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.Orientation;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.Resolution;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.ScanPreview;
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.ScanSize;
import com.hp.jetadvantage.link.api.scanner.ScanAttributesCaps;
import com.hp.jetadvantage.link.sample.scansample.MainActivity;
import com.hp.jetadvantage.link.sample.scansample.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Simple {@link PreferenceFragment} to set Scan Attributes and save into preferences.
 */
public final class ScanConfigureFragment extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "ScanConfigureFragment";

    // Preferences keys for ScanAttributes
    public static final String PREF_DESTINATION = "pref_destination";
    public static final String PREF_FILE_NAME = "pref_filename";
    public static final String PREF_FOLDER_NAME = "pref_folder_name";
    public static final String PREF_COLOR_MODE = "pref_colorMode";
    public static final String PREF_DUPLEX_MODE = "pref_duplexMode";
    public static final String PREF_RESOLUTION_TYPE = "pref_resolutionType";
    public static final String PREF_DOC_FORMAT = "pref_docFormat";
    public static final String PREF_ORG_SIZE = "pref_originalSize";
    public static final String PREF_CUSTOM_LENGTH = "pref_customLength";
    public static final String PREF_CUSTOM_WIDTH = "pref_customWidth";
    public static final String PREF_ORIENTATION = "pref_orientation";
    public static final String PREF_SCAN_PREVIEW = "pref_scanPreview";
    public static final String PREF_BACKGROUND_CLEANUP = "pref_backgroundCleanup";
    public static final String PREF_CONTRAST_ADJUSTMENT = "pref_contrastAdjustment";
    public static final String PREF_DARKNESS_ADJUSTMENT = "pref_darknessAdjustment";
    public static final String PREF_BLACK_IMAGE_REMOVAL_MODE = "pref_blankImageRemovalMode";
    public static final String PREF_COLOR_DROPOUT_MODE = "pref_colorDropoutMode";
    public static final String PREF_CROP_MODE = "pref_cropMode";
    public static final String PREF_PROGRESS_DIALOG_MODE = "pref_progressDialogMode";
    public static final String PREF_OUTPUT_QUALITY = "pref_outputQuality";
    public static final String PREF_TRANSMISSION_MODE = "pref_transmissionMode";
    public static final String PREF_JOB_ASSEMBLY_MODE = "pref_jobAssemblyMode";
    public static final String PREF_SHARPNESS_ADJUSTMENT = "pref_sharpnessAdjustment";
    public static final String PREF_MEDIA_WEIGHT_ADJUSTMENT = "pref_mediaWeightAdjustment";
    public static final String PREF_TEXT_PHOTO_OPTIMIZATION = "pref_textPhotoOptimization";

    public static final String PREF_MEDIA_SOURCE = "pref_mediaSource";
    public static final String PREF_MISFEED_DETECTION_MODE = "pref_misfeedDetectionMode";
    public static final String PREF_PDF_COMPRESSION= "pref_pdf_compression";
    public static final String PREF_OCR_LANGUAGE= "pref_ocr_language";
    public static final String PREF_PDF_PASSWORD = "pref_pdf_password";
    public static final String PREF_TIFF_COMPRESSION = "pref_tiff_compression";
    public static final String PREF_XPS_COMPRESSION = "pref_xps_compression";

    public static final String PREF_URI_HTTP = "pref_uri_http";
    public static final String PREF_URI_FTP = "pref_uri_ftp";
    public static final String PREF_URI_NETWORK_FOLDER = "pref_uri_network_folder";
    public static final String PREF_URI_USERNAME = "pref_uri_username";
    public static final String PREF_URI_PASSWORD = "pref_uri_password";
    public static final String PREF_URI_DOMAIN = "pref_uri_domain";

    public static final String PREF_EMAIL_TO = "pref_email_to";
    public static final String PREF_EMAIL_CC = "pref_email_cc";
    public static final String PREF_EMAIL_BCC = "pref_email_bcc";
    public static final String PREF_EMAIL_FROM = "pref_email_from";
    public static final String PREF_EMAIL_SUBJECT = "pref_email_subject";
    public static final String PREF_EMAIL_MESSAGE = "pref_email_message";
    public static final String PREF_EMAIL_SMTP = "pref_email_smtp";

    public static final String PREF_DESTINATION_HTTP_CATEGORY = "destination_http_category";
    public static final String PREF_DESTINATION_FTP_CATEGORY = "destination_ftp_category";
    public static final String PREF_DESTINATION_NETWORK_FOLDER_CATEGORY = "destination_network_folder_category";
    public static final String PREF_DESTINATION_EMAIL_CATEGORY = "destination_email_category";
    public static final String PREF_BASE_ATTRIBUTES_CATEGORY = "base_attributes_category";
    public static final String PREF_DESTINATION_FEEDBACK_CATEGORY = "destination_feedback_category";

    // Feedback / UI preferences
    public static final String PREF_MONITOR_JOB = "pref_monitorJob";
    public static final String PREF_SHOW_JOB_PROGRESS = "pref_showJobProgress";
    public static final String PREF_SETTINGS_UI = "pref_settingsUi";
    public static final String PREF_DEFAULT_EMAIL = "pref_default_email";

    // Preference for current job id
    public static final String CURRENT_JOB_ID = "pref_currentJobId";

    private ScanAttributesCaps mCaps;
    private PreferenceCategory mHttpCategory;
    private PreferenceCategory mFtpCategory;
    private PreferenceCategory mNetworkFolderCategory;
    private PreferenceCategory mEmailCategory;
    private PreferenceCategory mBaseAttributesCategory;
    private PreferenceCategory mFeedbackCategory;

    private EditTextPreference mFilenamePref;
    private FolderPathPreference mFoldernamePref;
    private EditTextPreference mFileUriHttpPref, mFileUriFtpPref, mFileUriNetworkFolderPref;
    private EditTextPreference mFileUriUsernamePref;
    private EditTextPreference mFileUriPasswordPref;
    private EditTextPreference mFileUriDomainPref;
    private EditTextPreference mEmailToPref;
    private EditTextPreference mEmailCcPref;
    private EditTextPreference mEmailBccPref;
    private CheckBoxPreference mEmailSmtpPref;
    private CheckBoxPreference mDefaultEmailPref;

    private ListPreference mTransmissionPref;
    private ListPreference mPDFCompressionPref;
    private ListPreference mOCRLanguagePref;
    private EditTextPreference mPDFPasswordPref;
    private ListPreference mTIFFCompressionPref;
    private ListPreference mXPSCompressionPref;
    private EditTextFloatPreference mCustomLengthPref;
    private EditTextFloatPreference mCustomWidthPref;

    private CheckBoxPreference mMonitorJobPref;
    private CheckBoxPreference mShowJobProgressPref;
    private CheckBoxPreference mSettingsUIPref;

    private boolean isSDKInitialized;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.scan_preferences);

        mFilenamePref = (EditTextPreference) findPreference(PREF_FILE_NAME);
        mFilenamePref.setText(null);
        mFoldernamePref = (FolderPathPreference) findPreference(PREF_FOLDER_NAME);
        mFoldernamePref.setText(null);

        mFileUriHttpPref = (EditTextPreference) findPreference(PREF_URI_HTTP);
        mFileUriHttpPref.setText(null);
        mFileUriFtpPref = (EditTextPreference) findPreference(PREF_URI_FTP);
        mFileUriFtpPref.setText(null);
        mFileUriNetworkFolderPref = (EditTextPreference) findPreference(PREF_URI_NETWORK_FOLDER);
        mFileUriNetworkFolderPref.setText(null);
        mFileUriUsernamePref = (EditTextPreference) findPreference(PREF_URI_USERNAME);
        mFileUriUsernamePref.setText(null);
        mFileUriPasswordPref = (EditTextPreference) findPreference(PREF_URI_PASSWORD);
        mFileUriPasswordPref.setText(null);
        mFileUriDomainPref = (EditTextPreference) findPreference(PREF_URI_DOMAIN);
        mFileUriDomainPref.setText(null);
        mDefaultEmailPref = (CheckBoxPreference) findPreference(PREF_DEFAULT_EMAIL);
        mDefaultEmailPref.setChecked(false);
        mDefaultEmailPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if ((boolean) newValue) {
                    FragmentManager fm = getFragmentManager();
                    new DefaultEmailSettingFragment().show(fm, getString(R.string.pref_default_email));
                }
                return true;
            }
        });

        mEmailToPref = (EditTextPreference) findPreference(PREF_EMAIL_TO);
        mEmailCcPref = (EditTextPreference) findPreference(PREF_EMAIL_CC);
        mEmailBccPref = (EditTextPreference) findPreference(PREF_EMAIL_BCC);
        mEmailSmtpPref = (CheckBoxPreference) findPreference(PREF_EMAIL_SMTP);
        mEmailSmtpPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if ((boolean) newValue) {
                    FragmentManager fm = getFragmentManager();
                    new EmailSmtpSettingFragment().show(fm, getString(R.string.pref_email_smtp_title));
                }
                return true;
            }
        });

        mTransmissionPref = (ListPreference) findPreference(PREF_TRANSMISSION_MODE);
        mPDFCompressionPref = (ListPreference) findPreference(PREF_PDF_COMPRESSION);
        mOCRLanguagePref = (ListPreference) findPreference(PREF_OCR_LANGUAGE);
        mPDFPasswordPref = (EditTextPreference) findPreference(PREF_PDF_PASSWORD);
        mTIFFCompressionPref = (ListPreference) findPreference(PREF_TIFF_COMPRESSION);
        mXPSCompressionPref = (ListPreference) findPreference(PREF_XPS_COMPRESSION);
        mCustomLengthPref = (EditTextFloatPreference) findPreference(PREF_CUSTOM_LENGTH);
        mCustomLengthPref.setLimits(0, 0);
        mCustomLengthPref.setText(null);
        mCustomWidthPref = (EditTextFloatPreference) findPreference(PREF_CUSTOM_WIDTH);
        mCustomWidthPref.setLimits(0, 0);
        mCustomWidthPref.setText(null);

        mHttpCategory = (PreferenceCategory) findPreference(PREF_DESTINATION_HTTP_CATEGORY);
        mFtpCategory = (PreferenceCategory) findPreference(PREF_DESTINATION_FTP_CATEGORY);
        mNetworkFolderCategory = (PreferenceCategory) findPreference(PREF_DESTINATION_NETWORK_FOLDER_CATEGORY);
        mEmailCategory = (PreferenceCategory) findPreference(PREF_DESTINATION_EMAIL_CATEGORY);
        mBaseAttributesCategory = (PreferenceCategory) findPreference(PREF_BASE_ATTRIBUTES_CATEGORY);
        mFeedbackCategory =  (PreferenceCategory) findPreference(PREF_DESTINATION_FEEDBACK_CATEGORY);

        mMonitorJobPref = (CheckBoxPreference) findPreference(PREF_MONITOR_JOB);
        mShowJobProgressPref = (CheckBoxPreference) findPreference(PREF_SHOW_JOB_PROGRESS);
        mSettingsUIPref = (CheckBoxPreference) findPreference(PREF_SETTINGS_UI);

        mMonitorJobPref.setChecked(true);
        mShowJobProgressPref.setChecked(true);
        mSettingsUIPref.setChecked(false);
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences prefs = getPreferenceScreen().getSharedPreferences();
        prefs.registerOnSharedPreferenceChangeListener(this);
        refreshAllPrefs(prefs);
    }

    @Override
    public void onPause() {
        super.onPause();

        SharedPreferences prefs = getPreferenceScreen().getSharedPreferences();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }
    /**
     * Refreshes al values in the fragment
     *
     * @param prefs {@link android.content.SharedPreferences}
     */
    private void refreshAllPrefs(final SharedPreferences prefs) {
        onSharedPreferenceChanged(prefs, PREF_DESTINATION);
        onSharedPreferenceChanged(prefs, PREF_FILE_NAME);
        onSharedPreferenceChanged(prefs, PREF_FOLDER_NAME);
        onSharedPreferenceChanged(prefs, PREF_COLOR_MODE);
        onSharedPreferenceChanged(prefs, PREF_DUPLEX_MODE);
        onSharedPreferenceChanged(prefs, PREF_ORIENTATION);
        onSharedPreferenceChanged(prefs, PREF_RESOLUTION_TYPE);
        onSharedPreferenceChanged(prefs, PREF_ORG_SIZE);
        onSharedPreferenceChanged(prefs, PREF_CUSTOM_LENGTH);
        onSharedPreferenceChanged(prefs, PREF_CUSTOM_WIDTH);
        onSharedPreferenceChanged(prefs, PREF_DOC_FORMAT);
        onSharedPreferenceChanged(prefs, PREF_SCAN_PREVIEW);
        onSharedPreferenceChanged(prefs, PREF_BACKGROUND_CLEANUP);
        onSharedPreferenceChanged(prefs, PREF_CONTRAST_ADJUSTMENT);
        onSharedPreferenceChanged(prefs, PREF_DARKNESS_ADJUSTMENT);
        onSharedPreferenceChanged(prefs, PREF_BLACK_IMAGE_REMOVAL_MODE);
        onSharedPreferenceChanged(prefs, PREF_COLOR_DROPOUT_MODE);
        onSharedPreferenceChanged(prefs, PREF_CROP_MODE);
        onSharedPreferenceChanged(prefs, PREF_PROGRESS_DIALOG_MODE);
        onSharedPreferenceChanged(prefs, PREF_OUTPUT_QUALITY);
        onSharedPreferenceChanged(prefs, PREF_TRANSMISSION_MODE);
        onSharedPreferenceChanged(prefs, PREF_JOB_ASSEMBLY_MODE);
        onSharedPreferenceChanged(prefs, PREF_SHARPNESS_ADJUSTMENT);
        onSharedPreferenceChanged(prefs, PREF_MEDIA_WEIGHT_ADJUSTMENT);
        onSharedPreferenceChanged(prefs, PREF_TEXT_PHOTO_OPTIMIZATION);
        onSharedPreferenceChanged(prefs, PREF_MEDIA_SOURCE);
        onSharedPreferenceChanged(prefs, PREF_MISFEED_DETECTION_MODE);

        onSharedPreferenceChanged(prefs, PREF_MONITOR_JOB);
        onSharedPreferenceChanged(prefs, PREF_SHOW_JOB_PROGRESS);
        onSharedPreferenceChanged(prefs, PREF_SETTINGS_UI);
        onSharedPreferenceChanged(prefs, PREF_DEFAULT_EMAIL);

        onSharedPreferenceChanged(prefs, PREF_EMAIL_TO);
        onSharedPreferenceChanged(prefs, PREF_EMAIL_CC);
        onSharedPreferenceChanged(prefs, PREF_EMAIL_BCC);
        onSharedPreferenceChanged(prefs, PREF_EMAIL_FROM);
        onSharedPreferenceChanged(prefs, PREF_EMAIL_SUBJECT);
        onSharedPreferenceChanged(prefs, PREF_EMAIL_MESSAGE);
        onSharedPreferenceChanged(prefs, PREF_EMAIL_SMTP);

        onSharedPreferenceChanged(prefs, PREF_PDF_COMPRESSION);
        onSharedPreferenceChanged(prefs, PREF_OCR_LANGUAGE);
        onSharedPreferenceChanged(prefs, PREF_PDF_PASSWORD);
        onSharedPreferenceChanged(prefs, PREF_TIFF_COMPRESSION);
        onSharedPreferenceChanged(prefs, PREF_XPS_COMPRESSION);
    }

    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
        final Preference preference = findPreference(key);

        if (PREF_DOC_FORMAT.equals(key)) {
            setColorMode(sharedPreferences, key);
            showOptionalPreference(sharedPreferences, key);
            fillFileOptionAttrCaps();
        } else if (PREF_ORG_SIZE.equals(key)){
            showCustomSizePreference(sharedPreferences, key);
        } else if (PREF_COLOR_MODE.equals(key)){
            fillFileOptionAttrCaps();
        } else if (PREF_DESTINATION.equals(key)) {
            final String entryStr = (String) ((ListPreference) preference).getEntry();
            final ScanAttributes.Destination destination =
                    entryStr == null ? ScanAttributes.Destination.ME : ScanAttributes.Destination.valueOf(entryStr);

            fillDocFormatPreferences(mCaps, PREF_DOC_FORMAT, destination);

            switch (destination) {
                case HTTP:
                    if (getPreferenceScreen().findPreference(PREF_DESTINATION_HTTP_CATEGORY) == null) {
                        getPreferenceScreen().addPreference(mHttpCategory);
                    }

                    mFeedbackCategory.removePreference(mDefaultEmailPref);
                    getPreferenceScreen().removePreference(mFtpCategory);
                    getPreferenceScreen().removePreference(mNetworkFolderCategory);
                    getPreferenceScreen().removePreference(mEmailCategory);

                    mFileUriUsernamePref = (EditTextPreference) findPreference(PREF_URI_USERNAME);
                    mFileUriUsernamePref.setText(null);
                    mFileUriPasswordPref = (EditTextPreference) findPreference(PREF_URI_PASSWORD);
                    mFileUriPasswordPref.setText(null);

                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_HTTP);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_USERNAME);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_PASSWORD);
                    supportTransmissionModeCaps(true);

                    mBaseAttributesCategory.removePreference(mFoldernamePref);
                    break;
                case FTP:
                    if (getPreferenceScreen().findPreference(PREF_DESTINATION_FTP_CATEGORY) == null) {
                        getPreferenceScreen().addPreference(mFtpCategory);
                    }

                    mFeedbackCategory.removePreference(mDefaultEmailPref);
                    getPreferenceScreen().removePreference(mHttpCategory);
                    getPreferenceScreen().removePreference(mNetworkFolderCategory);
                    getPreferenceScreen().removePreference(mEmailCategory);

                    mFileUriUsernamePref = (EditTextPreference) findPreference(PREF_URI_USERNAME);
                    mFileUriUsernamePref.setText(null);
                    mFileUriPasswordPref = (EditTextPreference) findPreference(PREF_URI_PASSWORD);
                    mFileUriPasswordPref.setText(null);

                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_FTP);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_USERNAME);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_PASSWORD);
                    supportTransmissionModeCaps(false);

                    mBaseAttributesCategory.removePreference(mFoldernamePref);
                    break;
                case NETWORK_FOLDER:
                    if (getPreferenceScreen().findPreference(PREF_DESTINATION_NETWORK_FOLDER_CATEGORY) == null) {
                        getPreferenceScreen().addPreference(mNetworkFolderCategory);
                    }

                    mFeedbackCategory.removePreference(mDefaultEmailPref);
                    getPreferenceScreen().removePreference(mHttpCategory);
                    getPreferenceScreen().removePreference(mFtpCategory);
                    getPreferenceScreen().removePreference(mEmailCategory);

                    mFileUriUsernamePref = (EditTextPreference) findPreference(PREF_URI_USERNAME);
                    mFileUriUsernamePref.setText(null);
                    mFileUriPasswordPref = (EditTextPreference) findPreference(PREF_URI_PASSWORD);
                    mFileUriPasswordPref.setText(null);
                    mFileUriDomainPref = (EditTextPreference) findPreference(PREF_URI_DOMAIN);
                    mFileUriDomainPref.setText(null);

                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_NETWORK_FOLDER);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_USERNAME);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_PASSWORD);
                    onSharedPreferenceChanged(sharedPreferences, PREF_URI_DOMAIN);

                    supportTransmissionModeCaps(false);

                    mBaseAttributesCategory.removePreference(mFoldernamePref);
                    break;

                case EMAIL:
                    if (getPreferenceScreen().findPreference(PREF_DESTINATION_EMAIL_CATEGORY) == null) {
                        getPreferenceScreen().addPreference(mEmailCategory);
                    }

                    mFeedbackCategory.removePreference(mDefaultEmailPref);
                    getPreferenceScreen().removePreference(mHttpCategory);
                    getPreferenceScreen().removePreference(mFtpCategory);
                    getPreferenceScreen().removePreference(mNetworkFolderCategory);

                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_TO);
                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_CC);
                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_BCC);
                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_FROM);
                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_SUBJECT);
                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_MESSAGE);
                    onSharedPreferenceChanged(sharedPreferences, PREF_EMAIL_SMTP);
                    supportTransmissionModeCaps(false);

                    mBaseAttributesCategory.removePreference(mFoldernamePref);
                    break;

                default:
                    getPreferenceScreen().removePreference(mHttpCategory);
                    getPreferenceScreen().removePreference(mFtpCategory);
                    getPreferenceScreen().removePreference(mNetworkFolderCategory);
                    getPreferenceScreen().removePreference(mEmailCategory);
                    mFeedbackCategory.addPreference(mDefaultEmailPref);
                    supportTransmissionModeCaps(false);
                    mBaseAttributesCategory.addPreference(mFoldernamePref);
                    break;
            }

            if (!Email.isSupported(getActivity())){
                mFeedbackCategory.removePreference(mDefaultEmailPref);
            }
        }

        if (preference instanceof ListPreference) {
            final String entry = (String) ((ListPreference) preference).getEntry();

            if (entry == null || entry.length() == 0) {
                ((ListPreference) preference).setValueIndex(0);
                preference.setSummary("%s");
            } else {
                preference.setSummary(entry);
            }
        } else if (preference instanceof EditTextPreference) {
            EditText edit = ((EditTextPreference) preference).getEditText();
            String text = ((EditTextPreference) preference).getText();
            if (text != null && edit.getTransformationMethod() != null) {
                text = edit.getTransformationMethod().getTransformation(text, edit).toString();
            }
            preference.setSummary(text);
        } else if (preference instanceof CheckBoxPreference) {
            if (PREF_MONITOR_JOB.equals(key)) {
                findPreference(PREF_SHOW_JOB_PROGRESS)
                        .setEnabled(((CheckBoxPreference) preference).isChecked());
            } else if (PREF_EMAIL_SMTP.equals(key) ||
                    PREF_DEFAULT_EMAIL.equals(key)) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                boolean value = sharedPref.getBoolean(key, false);
                ((CheckBoxPreference) preference).setChecked(value);
            }
        }
    }

    /**
     * Applies capabilities to configuration screen
     *
     * @param caps {@link com.hp.jetadvantage.link.api.scanner.ScanAttributesCaps}
     */
    public void loadCapabilities(final ScanAttributesCaps caps) {
        mCaps = caps;

        // Resolution - stored high to low
        ListPreference pref;

        // Load Resolutions
        pref = (ListPreference) findPreference(PREF_RESOLUTION_TYPE);
        ArrayList<CharSequence> resEntries = new ArrayList<>();
        ArrayList<CharSequence> resEntryValues = new ArrayList<>();

        for (Resolution res : caps.getResolutionList()) {
            resEntries.add(res.name());
            resEntryValues.add(res.name());
        }

        pref.setEntries(resEntries.toArray(new CharSequence[resEntries.size()]));
        pref.setEntryValues(resEntryValues.toArray(new CharSequence[resEntryValues.size()]));
        pref.setDefaultValue(Resolution.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        // Load color mode
        pref = (ListPreference) findPreference(PREF_COLOR_MODE);
        ArrayList<CharSequence> cmEntries = new ArrayList<>();
        ArrayList<CharSequence> cmEntryValues = new ArrayList<>();

        for (ColorMode cm : caps.getColorModeList()) {
            cmEntries.add(cm.name());
            cmEntryValues.add(cm.name());
        }

        pref.setEntries(cmEntries.toArray(new CharSequence[cmEntries.size()]));
        pref.setEntryValues(cmEntryValues.toArray(new CharSequence[cmEntryValues.size()]));
        pref.setDefaultValue(ColorMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        // Load duplex mode
        pref = (ListPreference) findPreference(PREF_DUPLEX_MODE);
        ArrayList<CharSequence> duEntries = new ArrayList<>();
        ArrayList<CharSequence> duEntryValues = new ArrayList<>();

        for (Duplex du : caps.getDuplexList()) {
            duEntries.add(du.name());
            duEntryValues.add(du.name());
        }

        pref.setEntries(duEntries.toArray(new CharSequence[duEntries.size()]));
        pref.setEntryValues(duEntryValues.toArray(new CharSequence[duEntryValues.size()]));
        pref.setDefaultValue(Duplex.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        //Load Original Orientation
        pref = (ListPreference) findPreference(PREF_ORIENTATION);
        ArrayList<CharSequence> orientationEntries = new ArrayList<>();
        ArrayList<CharSequence> orientationEntryValues = new ArrayList<>();

        for (Orientation ori : caps.getOrientationList()) {
            orientationEntries.add(ori.name()); //name
            orientationEntryValues.add(ori.name()); //value
        }
        pref.setEntries(orientationEntries.toArray(new CharSequence[orientationEntries.size()]));
        pref.setEntryValues(orientationEntryValues.toArray(new CharSequence[orientationEntryValues.size()]));
        pref.setDefaultValue(Orientation.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_BACKGROUND_CLEANUP);
        ArrayList<CharSequence> backgroundCleanupEntries = new ArrayList<>();
        ArrayList<CharSequence> backgroundCleanupValues = new ArrayList<>();

        for (ScanAttributes.BackgroundCleanup backgroundCleanup : caps.getBackgroundCleanupList()) {
            backgroundCleanupEntries.add(backgroundCleanup.name()); //name
            backgroundCleanupValues.add(backgroundCleanup.name()); //value
        }
        pref.setEntries(backgroundCleanupEntries.toArray(new CharSequence[backgroundCleanupEntries.size()]));
        pref.setEntryValues(backgroundCleanupValues.toArray(new CharSequence[backgroundCleanupValues.size()]));
        pref.setDefaultValue(ScanAttributes.BackgroundCleanup.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_CONTRAST_ADJUSTMENT);
        ArrayList<CharSequence> contrastAdjustmentEntries = new ArrayList<>();
        ArrayList<CharSequence> contrastAdjustmentValues = new ArrayList<>();

        for (ScanAttributes.ContrastAdjustment contrastAdjustment : caps.getContrastAdjustmentList()) {
            contrastAdjustmentEntries.add(contrastAdjustment.name()); //name
            contrastAdjustmentValues.add(contrastAdjustment.name()); //value
        }
        pref.setEntries(contrastAdjustmentEntries.toArray(new CharSequence[contrastAdjustmentEntries.size()]));
        pref.setEntryValues(contrastAdjustmentValues.toArray(new CharSequence[contrastAdjustmentValues.size()]));
        pref.setDefaultValue(ScanAttributes.ContrastAdjustment.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_DARKNESS_ADJUSTMENT);
        ArrayList<CharSequence> darknessAdjustmentEntries = new ArrayList<>();
        ArrayList<CharSequence> darknessAdjustmentValues = new ArrayList<>();

        for (ScanAttributes.DarknessAdjustment darknessAdjustment : caps.getDarknessAdjustmentList()) {
            darknessAdjustmentEntries.add(darknessAdjustment.name()); //name
            darknessAdjustmentValues.add(darknessAdjustment.name()); //value
        }
        pref.setEntries(darknessAdjustmentEntries.toArray(new CharSequence[darknessAdjustmentEntries.size()]));
        pref.setEntryValues(darknessAdjustmentValues.toArray(new CharSequence[darknessAdjustmentValues.size()]));
        pref.setDefaultValue(ScanAttributes.DarknessAdjustment.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_BLACK_IMAGE_REMOVAL_MODE);
        ArrayList<CharSequence> blackImageRemovalEntries = new ArrayList<>();
        ArrayList<CharSequence> blackImageRemovalValues = new ArrayList<>();

        for (ScanAttributes.BlankImageRemovalMode blankImageRemovalMode : caps.getBlankImageRemovalModeList()) {
            blackImageRemovalEntries.add(blankImageRemovalMode.name()); //name
            blackImageRemovalValues.add(blankImageRemovalMode.name()); //value
        }
        pref.setEntries(blackImageRemovalEntries.toArray(new CharSequence[blackImageRemovalEntries.size()]));
        pref.setEntryValues(blackImageRemovalValues.toArray(new CharSequence[blackImageRemovalValues.size()]));
        pref.setDefaultValue(ScanAttributes.BlankImageRemovalMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_COLOR_DROPOUT_MODE);
        ArrayList<CharSequence> colorDropoutModeEntries = new ArrayList<>();
        ArrayList<CharSequence> colorDropoutModeValues = new ArrayList<>();

        for (ScanAttributes.ColorDropoutMode colorDropoutMode : caps.getColorDropoutModeList()) {
            colorDropoutModeEntries.add(colorDropoutMode.name()); //name
            colorDropoutModeValues.add(colorDropoutMode.name()); //value
        }
        pref.setEntries(colorDropoutModeEntries.toArray(new CharSequence[colorDropoutModeEntries.size()]));
        pref.setEntryValues(colorDropoutModeValues.toArray(new CharSequence[colorDropoutModeValues.size()]));
        pref.setDefaultValue(ScanAttributes.ColorDropoutMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_CROP_MODE);
        ArrayList<CharSequence> cropModeEntries = new ArrayList<>();
        ArrayList<CharSequence> cropModeValues = new ArrayList<>();

        for (ScanAttributes.CropMode cropMode : caps.getCropModeList()) {
            cropModeEntries.add(cropMode.name()); //name
            cropModeValues.add(cropMode.name()); //value
        }
        pref.setEntries(cropModeEntries.toArray(new CharSequence[cropModeEntries.size()]));
        pref.setEntryValues(cropModeValues.toArray(new CharSequence[cropModeValues.size()]));
        pref.setDefaultValue(ScanAttributes.CropMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        // Load Original Size
        pref = (ListPreference) findPreference(PREF_ORG_SIZE);
        ArrayList<CharSequence> osEntries = new ArrayList<>();
        ArrayList<CharSequence> osEntryValues = new ArrayList<>();

        for (ScanSize os : caps.getScanSizeList()) {
            osEntries.add(os.name());
            osEntryValues.add(os.name());
        }

        pref.setEntries(osEntries.toArray(new CharSequence[osEntries.size()]));
        pref.setEntryValues(osEntryValues.toArray(new CharSequence[osEntryValues.size()]));
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_DESTINATION);
        ArrayList<CharSequence> destEntries = new ArrayList<>();
        ArrayList<CharSequence> destEntryValues = new ArrayList<>();

        for (ScanAttributes.Destination dest : caps.getDestinationList()) {
            destEntries.add(dest.name());
            destEntryValues.add(dest.name());
        }

        pref.setEntries(destEntries.toArray(new CharSequence[destEntries.size()]));
        pref.setEntryValues(destEntryValues.toArray(new CharSequence[destEntryValues.size()]));
        // ME is always presented
        pref.setDefaultValue(caps.getDestinationList().get(0));
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_SCAN_PREVIEW);
        ArrayList<CharSequence> scanPreviewEntries = new ArrayList<>();
        ArrayList<CharSequence> scanPreviewEntryValues = new ArrayList<>();

        for (ScanPreview scanPreview : caps.getScanPreviewList()) {
            scanPreviewEntries.add(scanPreview.name());
            scanPreviewEntryValues.add(scanPreview.name());
        }
        pref.setEntries(scanPreviewEntries.toArray(new CharSequence[scanPreviewEntries.size()]));
        pref.setEntryValues(scanPreviewEntryValues.toArray(new CharSequence[scanPreviewEntryValues.size()]));
        pref.setDefaultValue(ScanAttributes.ScanPreview.DEFAULT);
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_PROGRESS_DIALOG_MODE);
        ArrayList<CharSequence> progressDialogModeEntries = new ArrayList<>();
        ArrayList<CharSequence> progressDialogModeValues = new ArrayList<>();

        for (ScanAttributes.ProgressDialogMode progressDialogMode : caps.getProgressDialogModeList()) {
            progressDialogModeEntries.add(progressDialogMode.name()); //name
            progressDialogModeValues.add(progressDialogMode.name()); //value
        }
        pref.setEntries(progressDialogModeEntries.toArray(new CharSequence[progressDialogModeEntries.size()]));
        pref.setEntryValues(progressDialogModeValues.toArray(new CharSequence[progressDialogModeValues.size()]));
        pref.setDefaultValue(ScanAttributes.ProgressDialogMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_OUTPUT_QUALITY);
        ArrayList<CharSequence> outputQualityEntries = new ArrayList<>();
        ArrayList<CharSequence> outputQualityValues = new ArrayList<>();

        for (ScanAttributes.OutputQuality outputQuality : caps.getOutputQualityList()) {
            outputQualityEntries.add(outputQuality.name()); //name
            outputQualityValues.add(outputQuality.name()); //value
        }
        pref.setEntries(outputQualityEntries.toArray(new CharSequence[outputQualityEntries.size()]));
        pref.setEntryValues(outputQualityValues.toArray(new CharSequence[outputQualityValues.size()]));
        pref.setDefaultValue(ScanAttributes.OutputQuality.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_JOB_ASSEMBLY_MODE);
        ArrayList<CharSequence> jobAssemblyModeEntries = new ArrayList<>();
        ArrayList<CharSequence> jobAssemblyModeValues = new ArrayList<>();

        for (ScanAttributes.JobAssemblyMode jobAssemblyMode : caps.getJobAssemblyModeList()) {
            jobAssemblyModeEntries.add(jobAssemblyMode.name()); //name
            jobAssemblyModeValues.add(jobAssemblyMode.name()); //value
        }
        pref.setEntries(jobAssemblyModeEntries.toArray(new CharSequence[jobAssemblyModeEntries.size()]));
        pref.setEntryValues(jobAssemblyModeValues.toArray(new CharSequence[jobAssemblyModeValues.size()]));
        pref.setDefaultValue(ScanAttributes.JobAssemblyMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_SHARPNESS_ADJUSTMENT);
        ArrayList<CharSequence> sharpnessAdjustmentEntries = new ArrayList<>();
        ArrayList<CharSequence> sharpnessAdjustmentValues = new ArrayList<>();

        for (ScanAttributes.SharpnessAdjustment sharpnessAdjustment : caps.getSharpnessAdjustmentList()) {
            sharpnessAdjustmentEntries.add(sharpnessAdjustment.name()); //name
            sharpnessAdjustmentValues.add(sharpnessAdjustment.name()); //value
        }
        pref.setEntries(sharpnessAdjustmentEntries.toArray(new CharSequence[sharpnessAdjustmentEntries.size()]));
        pref.setEntryValues(sharpnessAdjustmentValues.toArray(new CharSequence[sharpnessAdjustmentValues.size()]));
        pref.setDefaultValue(ScanAttributes.SharpnessAdjustment.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_MEDIA_WEIGHT_ADJUSTMENT);
        ArrayList<CharSequence> mediaWeightAdjustmentEntries = new ArrayList<>();
        ArrayList<CharSequence> mediaWeightAdjustmentValues = new ArrayList<>();

        for (ScanAttributes.MediaWeightAdjustment mediaWeightAdjustment : caps.getMediaWeightAdjustmentList()) {
            mediaWeightAdjustmentEntries.add(mediaWeightAdjustment.name()); //name
            mediaWeightAdjustmentValues.add(mediaWeightAdjustment.name()); //value
        }
        pref.setEntries(mediaWeightAdjustmentEntries.toArray(new CharSequence[mediaWeightAdjustmentEntries.size()]));
        pref.setEntryValues(mediaWeightAdjustmentValues.toArray(new CharSequence[mediaWeightAdjustmentValues.size()]));
        pref.setDefaultValue(ScanAttributes.MediaWeightAdjustment.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_TEXT_PHOTO_OPTIMIZATION);
        ArrayList<CharSequence> textPhotoOptimizationEntries = new ArrayList<>();
        ArrayList<CharSequence> textPhotoOptimizationValues = new ArrayList<>();

        for (ScanAttributes.TextPhotoOptimization textPhotoOptimization : caps.getTextPhotoOptimizationList()) {
            textPhotoOptimizationEntries.add(textPhotoOptimization.name()); //name
            textPhotoOptimizationValues.add(textPhotoOptimization.name()); //value
        }
        pref.setEntries(textPhotoOptimizationEntries.toArray(new CharSequence[textPhotoOptimizationEntries.size()]));
        pref.setEntryValues(textPhotoOptimizationValues.toArray(new CharSequence[textPhotoOptimizationValues.size()]));
        pref.setDefaultValue(ScanAttributes.TextPhotoOptimization.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_MEDIA_SOURCE);
        ArrayList<CharSequence> mediaSourceEntries = new ArrayList<>();
        ArrayList<CharSequence> mediaSourceValues = new ArrayList<>();

        for (ScanAttributes.MediaSource mediaSource : caps.getMediaSourceList()) {
            mediaSourceEntries.add(mediaSource.name()); //name
            mediaSourceValues.add(mediaSource.name()); //value
        }
        pref.setEntries(mediaSourceEntries.toArray(new CharSequence[mediaSourceEntries.size()]));
        pref.setEntryValues(mediaSourceValues.toArray(new CharSequence[mediaSourceValues.size()]));
        pref.setDefaultValue(ScanAttributes.MediaSource.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        pref = (ListPreference) findPreference(PREF_MISFEED_DETECTION_MODE);
        ArrayList<CharSequence> misfeedDetectionModeEntries = new ArrayList<>();
        ArrayList<CharSequence> misfeedDetectionModeValues = new ArrayList<>();

        for (ScanAttributes.MisfeedDetectionMode misfeedDetectionMode : caps.getMisfeedDetectionModeList()) {
            misfeedDetectionModeEntries.add(misfeedDetectionMode.name()); //name
            misfeedDetectionModeValues.add(misfeedDetectionMode.name()); //value
        }
        pref.setEntries(misfeedDetectionModeEntries.toArray(new CharSequence[misfeedDetectionModeEntries.size()]));
        pref.setEntryValues(misfeedDetectionModeValues.toArray(new CharSequence[misfeedDetectionModeValues.size()]));
        pref.setDefaultValue(ScanAttributes.MisfeedDetectionMode.DEFAULT.name());
        pref.setValueIndex(0);
        pref.setSummary("%s");

        mCustomLengthPref.setLimits(caps.getCustomLengthRange().getLowerBound(), caps.getCustomLengthRange().getUpperBound());
        mCustomWidthPref.setLimits(caps.getCustomWidthRange().getLowerBound(), caps.getCustomWidthRange().getUpperBound());

        // Doc Formats for different destinations
        fillDocFormatPreferences(caps, PREF_DOC_FORMAT, ScanAttributes.Destination.ME);
    }

    /**
     * Fills Doc format preferences values
     *  @param caps          {@link ScanAttributesCaps} to take data from
     * @param prefDocFormat Strin shared preferences key
     * @param dest          {@link ScanAttributes.Destination} to
     */
    private void fillDocFormatPreferences(ScanAttributesCaps caps, String prefDocFormat, ScanAttributes.Destination dest) {
        if (caps == null) {
            return;
        }

        ListPreference pref = (ListPreference) findPreference(prefDocFormat);

        ArrayList<CharSequence> dfEntries = new ArrayList<>();
        ArrayList<CharSequence> dfEntryValues = new ArrayList<>();

        // Load Doc format
        for (ScanAttributes.DocumentFormat df : caps.getDocumentFormatList(dest)) {
            dfEntries.add(df.name());
            dfEntryValues.add(df.name());
        }

        if (pref != null) {
            pref.setEntries(dfEntries.toArray(new CharSequence[dfEntries.size()]));
            pref.setEntryValues(dfEntryValues.toArray(new CharSequence[dfEntryValues.size()]));
            pref.setValueIndex(0);
            pref.setSummary("%s");
        }
    }

    private void setColorMode(SharedPreferences sharedPreferences, final String key) {
        final DocumentFormat docFormat = DocumentFormat.valueOf(sharedPreferences.getString(key, DocumentFormat.DEFAULT.name()));
        final ListPreference colorModeList =
                (ListPreference) findPreference(com.hp.jetadvantage.link.sample.scansample.fragments.ScanConfigureFragment.PREF_COLOR_MODE);

        if (mCaps != null) {
            List<CharSequence> entries = new ArrayList<>();
            entries.add(ColorMode.DEFAULT.name());

            for (Map.Entry<ColorMode, List<DocumentFormat>> entry : mCaps.getDocumentFormatsByColorMode().entrySet()) {
                if (entry.getValue().contains(docFormat)
                        && entry.getKey() != ColorMode.DEFAULT) {
                    entries.add(entry.getKey().name());
                }
            }

            CharSequence[] entriesArray = entries.toArray(new CharSequence[entries.size()]);
            colorModeList.setEntries(entriesArray);
            colorModeList.setEntryValues(entriesArray);
            colorModeList.setValueIndex(0);
        } else {
            colorModeList.setEntries(R.array.pref_default_entries);
            colorModeList.setEntryValues(R.array.pref_default_entries);
        }
    }

    private void showOptionalPreference(SharedPreferences preferences, String key){
        mBaseAttributesCategory.removePreference(mPDFCompressionPref);
        mBaseAttributesCategory.removePreference(mOCRLanguagePref);
        mBaseAttributesCategory.removePreference(mPDFPasswordPref);
        mBaseAttributesCategory.removePreference(mTIFFCompressionPref);
        mBaseAttributesCategory.removePreference(mXPSCompressionPref);

        DocumentFormat docFormat = DocumentFormat.valueOf(preferences.getString(key, DocumentFormat.DEFAULT.name()));
        switch (docFormat){
            case PDF:
                mBaseAttributesCategory.addPreference(mPDFCompressionPref);
                mBaseAttributesCategory.addPreference(mPDFPasswordPref);
                break;
            case OCR_PDF_TEXT_UNDER_IMAGE:
                mBaseAttributesCategory.addPreference(mPDFCompressionPref);
                mBaseAttributesCategory.addPreference(mOCRLanguagePref);
                mBaseAttributesCategory.addPreference(mPDFPasswordPref);
                break;
            case MTIFF:
            case TIFF:
                mBaseAttributesCategory.addPreference(mTIFFCompressionPref);
                break;
            case OCR_PDF_A_TEXT_UNDER_IMAGE:
                mBaseAttributesCategory.addPreference(mPDFCompressionPref);
                mBaseAttributesCategory.addPreference(mOCRLanguagePref);
                break;
            case OCR_CSV:
            case OCR_HTML:
            case OCR_RTF:
            case OCR_TEXT:
            case OCR_UNICODE_TEXT:
                mBaseAttributesCategory.addPreference(mOCRLanguagePref);
                break;
            case PDF_A:
                mBaseAttributesCategory.addPreference(mPDFCompressionPref);
                break;
            case XPS:
                mBaseAttributesCategory.addPreference(mXPSCompressionPref);
                break;
        }
    }

    private void showCustomSizePreference(SharedPreferences preferences, String key){
        ScanSize scanSize = ScanSize.valueOf(preferences.getString(key, ScanSize.DEFAULT.name()));

        if (scanSize == ScanSize.CUSTOM) {
            mBaseAttributesCategory.addPreference(mCustomLengthPref);
            mBaseAttributesCategory.addPreference(mCustomWidthPref);
        } else {
            mBaseAttributesCategory.removePreference(mCustomLengthPref);
            mBaseAttributesCategory.removePreference(mCustomWidthPref);
        }
    }

    private void supportTransmissionModeCaps(boolean isSupported){
        if(isSupported){
            mBaseAttributesCategory.addPreference(mTransmissionPref);
            if(mTransmissionPref != null) {
                ArrayList<CharSequence> transmissionModeEntries = new ArrayList<>();
                ArrayList<CharSequence> transmissionModeValues = new ArrayList<>();

                for (ScanAttributes.TransmissionMode transmissionMode : mCaps.getTransmissionModeList()) {
                    transmissionModeEntries.add(transmissionMode.name()); //name
                    transmissionModeValues.add(transmissionMode.name()); //value
                }
                mTransmissionPref.setEntries(transmissionModeEntries.toArray(new CharSequence[transmissionModeEntries.size()]));
                mTransmissionPref.setEntryValues(transmissionModeValues.toArray(new CharSequence[transmissionModeValues.size()]));
                mTransmissionPref.setDefaultValue(ScanAttributes.TransmissionMode.DEFAULT.name());
                mTransmissionPref.setValueIndex(0);
                mTransmissionPref.setSummary("%s");
            }
        } else {
            mBaseAttributesCategory.removePreference(mTransmissionPref);
            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(PREF_TRANSMISSION_MODE, ScanAttributes.TransmissionMode.DEFAULT.name());
            editor.commit();
        }
    }

    private void fillFileOptionAttrCaps() {
        if(isSDKInitialized()) {
            ListPreference docPref = (ListPreference) findPreference(PREF_DOC_FORMAT);
            String docEntry = (String) docPref.getEntry();
            DocumentFormat docFormat = docEntry == null ? DocumentFormat.DEFAULT : ScanAttributes.DocumentFormat.valueOf(docEntry);

            ListPreference colorPref = (ListPreference) findPreference(PREF_COLOR_MODE);
            String colorEntry = (String) colorPref.getEntry();
            ColorMode colorMode = colorEntry == null ? ColorMode.DEFAULT : ScanAttributes.ColorMode.valueOf(colorEntry);

            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

            FileOptionsAttributesCaps fileOptionsAttrCaps = ((MainActivity) getActivity()).requestFileOptionsCapabilities(colorMode, docFormat);

            EditTextPreference editPref = (EditTextPreference) findPreference(PREF_PDF_PASSWORD);
            boolean isPdfEncryptionSupport = fileOptionsAttrCaps.isPdfEncryptionPasswordSupported();
            if (editPref != null && isPdfEncryptionSupport) {
                String password = mPrefs.getString(PREF_PDF_PASSWORD, null);
                editPref.setText(password);
            } else {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(PREF_PDF_PASSWORD, null);
                editor.commit();
            }

            ListPreference pref = (ListPreference) findPreference(PREF_OCR_LANGUAGE);

            ArrayList<CharSequence> ocrEntries = new ArrayList<>();
            ArrayList<CharSequence> ocrEntryValues = new ArrayList<>();

            for (FileOptionsAttributes.OcrLanguage language : fileOptionsAttrCaps.getOcrLanguageList()) {
                if(language != null && language.name() != null) {
                    ocrEntries.add(language.name());
                    ocrEntryValues.add(language.name());
                }
            }

            if (pref != null) {
                pref.setEntries(ocrEntries.toArray(new CharSequence[ocrEntries.size()]));
                pref.setEntryValues(ocrEntryValues.toArray(new CharSequence[ocrEntryValues.size()]));
                pref.setValueIndex(0);
                pref.setSummary("%s");
            } else {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(PREF_OCR_LANGUAGE, FileOptionsAttributes.OcrLanguage.DEFAULT.name());
                editor.commit();
            }

            pref = (ListPreference) findPreference(PREF_PDF_COMPRESSION);

            ArrayList<CharSequence> pdfCompEntries = new ArrayList<>();
            ArrayList<CharSequence> pdfCompEntryValues = new ArrayList<>();

            for (FileOptionsAttributes.PdfCompressionMode compressionMode : fileOptionsAttrCaps.getPdfCompressionModeList()) {
                pdfCompEntries.add(compressionMode.name());
                pdfCompEntryValues.add(compressionMode.name());
            }

            if (pref != null) {
                pref.setEntries(pdfCompEntries.toArray(new CharSequence[pdfCompEntries.size()]));
                pref.setEntryValues(pdfCompEntryValues.toArray(new CharSequence[pdfCompEntryValues.size()]));
                pref.setValueIndex(0);
                pref.setSummary("%s");
            } else {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(PREF_PDF_COMPRESSION, FileOptionsAttributes.PdfCompressionMode.DEFAULT.name());
                editor.commit();
            }

            pref = (ListPreference) findPreference(PREF_TIFF_COMPRESSION);

            ArrayList<CharSequence> tiffCompEntries = new ArrayList<>();
            ArrayList<CharSequence> tiffCompEntryValues = new ArrayList<>();

            for (FileOptionsAttributes.TiffCompressionMode tiffComp : fileOptionsAttrCaps.getTiffCompressionModeList()) {
                tiffCompEntries.add(tiffComp.name());
                tiffCompEntryValues.add(tiffComp.name());
            }

            if (pref != null) {
                pref.setEntries(tiffCompEntries.toArray(new CharSequence[tiffCompEntries.size()]));
                pref.setEntryValues(tiffCompEntryValues.toArray(new CharSequence[tiffCompEntryValues.size()]));
                pref.setValueIndex(0);
                pref.setSummary("%s");
            } else {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(PREF_TIFF_COMPRESSION, FileOptionsAttributes.TiffCompressionMode.DEFAULT.name());
                editor.commit();
            }

            pref = (ListPreference) findPreference(PREF_XPS_COMPRESSION);

            ArrayList<CharSequence> xpsCompEntries = new ArrayList<>();
            ArrayList<CharSequence> xpsCompEntryValues = new ArrayList<>();

            for (FileOptionsAttributes.XpsCompressionMode xpsComp : fileOptionsAttrCaps.getXpsCompressionModeList()) {
                xpsCompEntries.add(xpsComp.name());
                xpsCompEntryValues.add(xpsComp.name());
            }

            if (pref != null) {
                pref.setEntries(xpsCompEntries.toArray(new CharSequence[xpsCompEntries.size()]));
                pref.setEntryValues(xpsCompEntryValues.toArray(new CharSequence[xpsCompEntryValues.size()]));
                pref.setValueIndex(0);
                pref.setSummary("%s");
            } else {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(PREF_XPS_COMPRESSION, FileOptionsAttributes.XpsCompressionMode.DEFAULT.name());
                editor.commit();
            }
        }
    }

    public boolean isSDKInitialized() {
        return isSDKInitialized;
    }

    public void setSDKInitialized(boolean SDKInitialized) {
        isSDKInitialized = SDKInitialized;
    }

    public ScanAttributesCaps getCaps() {
        return mCaps;
    }
}
