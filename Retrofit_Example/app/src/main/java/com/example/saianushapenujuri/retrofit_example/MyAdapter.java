package com.example.saianushapenujuri.retrofit_example;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<AndroidVersions> mAndroidList;

    public MyAdapter(ArrayList<AndroidVersions> android) {
        this.mAndroidList = android;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder viewHolder, int i) {

        viewHolder.nameTxt.setText(mAndroidList.get(i).getName());
        viewHolder.versionTxt.setText(mAndroidList.get(i).getVersion());
    }

    @Override
    public int getItemCount() {
        return mAndroidList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public TextView versionTxt;

        public ViewHolder(View view) {
            super(view);

            nameTxt = view.findViewById(R.id.text_name);
            versionTxt = view.findViewById(R.id.text_version);

        }
    }

}

