package com.example.saianushapenujuri.retrofit_example;


import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {

    @GET("bins/1f8t7w")
    Call<JSONResponse> getJSON();
}
